'use strict';
var mongoose = require('mongoose');

var DataSchema = mongoose.Schema({
  title: String,
  description: String
});

module.exports = mongoose.model('Data', DataSchema);