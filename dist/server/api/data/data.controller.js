/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /data              ->  index
 * GET     /data/:id          ->  show
 * POST    /data              ->  create
 * PUT     /data/:id          ->  update
 * DELETE  /data/:id          ->  destroy
 */

'use strict';

var Data = require('./data.model');

function handleError(res, err) {
  return res.send(500, err);
}

exports.index = function(req, res) {
  Data.find(function(err, datas) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(datas);
  });
};

exports.show = function(req, res) {
  Data.findById(req.params.id, function(err, data) {
    if(err) { return handleError(res, err); }
    if(!data) { return res.sendStatus(404); }
    return res.status(200).json(data);
  });
};

exports.update = function(req, res) {
  Data.findById(req.params.id, function(err, data) {
    if(err) { return handleError(res, err); }
    if(!data) { return res.sendStatus(404); }
    data.update(req.body).exec();
  });
};

exports.create = function(req, res) {
  Data.create(req.body, function(err) {
    if(err) { return handleError(res, err); }
    return res.sendStatus(200);
  });
};

exports.destroy = function (req, res) {
  Data.findById(req.params.id, function(err, data) {
    if(!data) { return res.sendStatus(404); }
    if(err) { return handleError(res, err); }
    data.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(200);
    });
  });
};
