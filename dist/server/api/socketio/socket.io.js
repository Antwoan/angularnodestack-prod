'use strict';

module.exports = function(http) {
  var io = require('socket.io')(http);
  io.on('connection', function(socket) {
    console.log('User connected: ' + socket.handshake.address);

    socket.on('chat message', function(msg) {
      io.emit('chat message', msg);
    });
    
    socket.on('disconnect', function() {
      console.log('User disconnected');
    });
  });
};