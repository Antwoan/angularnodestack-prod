'use strict';

var Data = require('./data/data.model.js');

exports.seedData = function() {
  Data.find({}).remove(function(error) {
    if(error) { return error; }
    Data.create({ title: 'Marie',  description: 'Liten och go.' });
    Data.create({ title: 'Anton',  description: 'Skön kille.' });
    Data.create({ title: 'Johannes Junior',  description: 'Snabb.' });
  });
};