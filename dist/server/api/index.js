'use strict';

module.exports = function(app, http) {
  app.use('/api/datas', require('./data'));
  require('./socketio/socket.io')(http);
};
