'use strict';

var mongoose = require('mongoose');
var dbName = 'data';

var mongoConnection = process.env.OPENSHIFT_MONGODB_DB_URL ? process.env.OPENSHIFT_MONGODB_DB_URL + dbName : 'mongodb://localhost/' + dbName;

mongoose.connect(mongoConnection);

exports.db = mongoose.connection;