'use strict';
module.exports = function(app) {
  return {
    route: require('./routes')(app),
    db: require('./database').db,
    env: require('./environment')
  };
};