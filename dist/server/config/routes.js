'use strict';
var path = require('path');

module.exports = function(app) {
  app.route('/*')
    .get(function(req, res) {
      // Just send the index.html for other files to support HTML5Mode
      var root = path.join(__dirname, '..', '..', 'client');
      res.sendFile('index.html', { root: root });
  });
};