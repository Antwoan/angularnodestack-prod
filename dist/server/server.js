'use strict';
// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express    = require('express');
var morgan     = require('morgan');
var bodyParser = require('body-parser');
var app        = express();
var http       = require('http').Server(app);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./api')(app, http);

process.env.NODE_ENV === 'development' ? app.use(express.static('client')) : app.use(express.static('../client'));

var config = require('./config')(app);
var db = config.db;
var port = config.env.port;

db.once('open', function() {
  console.log('Successfully connected to mongodb!');
  // seed.seedData();
});

app.use(morgan('combined'));
http.listen(port, function() {
  console.log('Magic happens at port %d.', port);
});